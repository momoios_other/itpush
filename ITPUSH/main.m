//
//  main.m
//  ITPUSH
//
//  Created by eyliu on 2015/1/6.
//  Copyright (c) 2015年 momoshop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
