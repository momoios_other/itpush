//
//  CustomCell.h
//  ITPUSH
//
//  Created by eyliu on 2015/1/7.
//  Copyright (c) 2015年 momoshop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell{
    IBOutlet UIImageView *userImage;
    IBOutlet UILabel *userName;
    IBOutlet UILabel *sendTime;
    IBOutlet UILabel *contents;
    IBOutlet UIImageView *MessageImageView;
}

@property (nonatomic, strong) UIImageView *userImage;
@property (nonatomic, strong) UILabel *userName;
@property (nonatomic, strong) UILabel *sendTime;
@property (nonatomic, strong) UILabel *contents;
@property (nonatomic, strong) UIImageView *MessageImageView;

@end
