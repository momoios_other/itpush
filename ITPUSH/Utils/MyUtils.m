//
//  MyUtils.m
//  SCM_MOMOSHOP
//
//  Created by eyliu on 2015/7/20.
//  Copyright (c) 2015年 momo. All rights reserved.
//

#import "MyUtils.h"
#include <arpa/inet.h>
#import <sys/utsname.h>

#define DATE_BEGIN -13
#define DATE_END 1

@implementation MyUtils
static NSMutableArray * _singletonDateRange;

#pragma mark - check string Format
+(BOOL) isnil:(id)value{
    if (value == nil){
        return YES;
    }
    return NO;
}

+(BOOL) isEmptyString : (NSString *)string
{
    if([string length] == 0 || [string isKindOfClass:[NSNull class]] ||
       [string isEqualToString:@""]||[string  isEqualToString:NULL]  ||
       string == nil)
    {
        return YES;         //IF String Is An Empty String
    }
    return NO;
}


+ (BOOL) isValidIpAddress:(NSString *)ip
{
    const char *utf8 = [ip UTF8String];
    
    // Check valid IPv4.
    struct in_addr dst;
    int success = inet_pton(AF_INET, utf8, &(dst.s_addr));
    if (success != 1) {
        // Check valid IPv6.
        struct in6_addr dst6;
        success = inet_pton(AF_INET6, utf8, &dst6);
    }
    return (success == 1);
}

#pragma mark - get iOS Device Info
+ (NSString * )getiDeviceType {
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

+(NSString *) getiDeviceName_identifierForVendor {
    NSString *sUDID =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    return sUDID;
}

#pragma mark - set RGB color
+(UIColor*)SetColorWithHexString:(NSString*)hex andAlpha:(float)alphaValue
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:alphaValue];
}

+(NSDate *)getTimeDate_Begin:(NSString *)beginTime{

    NSArray *Ary_time = [beginTime componentsSeparatedByString:@"/"];
    NSString *getTime = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00", Ary_time[0], [Ary_time[1] intValue]>9?Ary_time[1]:[NSString stringWithFormat:@"0%@",Ary_time[1]], [Ary_time[2] intValue]>9?Ary_time[2]:[NSString stringWithFormat:@"0%@",Ary_time[2]]];
    
    NSDateFormatter *dateformat=[[NSDateFormatter alloc]init];
    [dateformat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateformat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];
    NSDate *dateStr=[dateformat dateFromString:getTime];
    
    return dateStr;
}

+(NSDate *)getTimeDate_End:(NSString *)endTime{

    NSArray *Ary_time = [endTime componentsSeparatedByString:@"/"];
    NSString *getTime = [NSString stringWithFormat:@"%@-%@-%@ 23:59:59", Ary_time[0], [Ary_time[1] intValue]>9?Ary_time[1]:[NSString stringWithFormat:@"0%@",Ary_time[1]], [Ary_time[2] intValue]>9?Ary_time[2]:[NSString stringWithFormat:@"0%@",Ary_time[2]]];
    
    NSDateFormatter *dateformat=[[NSDateFormatter alloc]init];
    [dateformat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateformat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];
    NSDate *dateStr=[dateformat dateFromString:getTime];
    
    return dateStr;
}

+(NSString *)getTimeStampFromDate:(NSDate *)date{
    return [NSString stringWithFormat:@"%.f", [date timeIntervalSince1970]*1000];
}

+(NSDate *)getDateFromTimeStamp:(NSString *)timeStamp{
    NSTimeInterval _interval=[timeStamp doubleValue] / 1000;
    return [NSDate dateWithTimeIntervalSince1970:_interval];
}

+(NSString *)getTimeFormatStringFromDate:(NSDate *)date{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    return [[format stringFromDate:date] substringToIndex:16];
}

#pragma mark - get image Encode
+ (NSString *)getEncodeToBase64String:(UIImage *)image {
    //return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return [UIImageJPEGRepresentation(image, 0.3) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

#pragma mark - get Date Info
+(NSMutableArray *)getSingletonDateRange{
    if (Nil == _singletonDateRange) {
        _singletonDateRange = [[NSMutableArray alloc] init];
        
        for (int index = DATE_BEGIN; index< DATE_END; index++) {
            [_singletonDateRange addObject:[NSNumber numberWithInt:index]];
        }
    }
    return _singletonDateRange;
}


+(NSMutableArray *)getQueryDateRange{
    
    // set format
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    // 為了傳送需要時間型式
    NSDateFormatter *formatter_send = [[NSDateFormatter alloc] init];
    [formatter_send setDateFormat:@"yyyy-MM-dd"];
    
    
    NSDate *now = [NSDate date];
    //set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSMutableArray *getQueryMary = [[NSMutableArray alloc] init];
    
    for (int index =0; index < [[MyUtils getSingletonDateRange] count]; index++) {
        [components setDay:[[[MyUtils getSingletonDateRange] objectAtIndex:index] intValue]];
        NSDate *Day = [gregorian dateByAddingComponents:components toDate:now options:0];
        
        // 取得民國年
        [formatter setDateFormat:@"yyyy"];
        int int_year = [[formatter stringFromDate:Day] intValue];
        // 取得月
        [formatter setDateFormat:@"MM"];
        int int_month = [[formatter stringFromDate:Day] intValue];
        // 取得日
        [formatter setDateFormat:@"dd"];
        int int_day = [[formatter stringFromDate:Day] intValue];
        // 取得星期幾
        NSDateComponents *weekDayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:Day];
        NSString *str_weekday;
        switch ([weekDayComponents weekday]) {
            case 1:
                str_weekday= @"日";
                break;
            case 2:
                str_weekday= @"一";
                break;
            case 3:
                str_weekday= @"二";
                break;
            case 4:
                str_weekday= @"三";
                break;
            case 5:
                str_weekday= @"四";
                break;
            case 6:
                str_weekday= @"五";
                break;
            case 7:
                str_weekday= @"六";
                break;
            default:
                break;
        }
        
        [getQueryMary addObject:[NSString stringWithFormat:@"%i/%i/%i",int_year,int_month,int_day]];
    }
    
    return getQueryMary;
}

+(void)setNSUserDefaultsKey:(NSString *)key andValue:(NSString *)value{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString *)getStringFromNSUserDefaults:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] stringForKey:key];
}

+(NSInteger)getINTFromNSUserDefaults:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] integerForKey:key];
}

+(float)getFloatFromNSUserDefaults:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] floatForKey:key];
}

+ (CGFloat)getMaxValue:(CGFloat)firstValue withSecondValue:(CGFloat)secondValue{
    return (firstValue>secondValue)?firstValue:secondValue;
}

+ (CGFloat)getMinValue:(CGFloat)firstValue withSecondValue:(CGFloat)secondValue{
    return (firstValue<secondValue)?firstValue:secondValue;
}

+ (CGFloat)getStringWidth:(NSString *)string withFontSize:(int)fontSize {
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:fontSize];
    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                     NSForegroundColorAttributeName: [UIColor blackColor]};
    const CGSize textSize = [string sizeWithAttributes: userAttributes];
    return textSize.width+15;
}

+(void)shareLineWithURL:(NSString *)msgWithURL{
    //為處理utf-8，故先編碼後再parse
    NSString *encodeUrl = [msgWithURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *parseUrl = [NSURL URLWithString:encodeUrl];
    
    NSString *parseURL = [parseUrl query];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"line://msg/text/%@", [[parseURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
}

@end
