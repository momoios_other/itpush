//
//  MyUtils.h
//  SCM_MOMOSHOP
//
//  Created by eyliu on 2015/7/20.
//  Copyright (c) 2015年 momo. All rights reserved.
//
//  V1.150707

#import <Foundation/Foundation.h>

@interface MyUtils : NSObject

#pragma mark - check string Format
+(BOOL) isnil:(id)value;
+(BOOL) isEmptyString:(NSString *)string;
+(BOOL) isValidIpAddress:(NSString *)ip;
+(CGFloat)getStringWidth:(NSString *)string withFontSize:(int)fontSize;

#pragma mark - get iOS Device Info
+(NSString *) getiDeviceType;
+(NSString *) getiDeviceName_identifierForVendor;

#pragma mark - set RGB color
+(UIColor*)SetColorWithHexString:(NSString*)hex andAlpha:(float)alphaValue;

#pragma mark - get Date Info
+(NSArray *)getSingletonDateRange;
+(NSMutableArray *)getQueryDateRange;
+(NSDate *)getTimeDate_Begin:(NSString *)beginTime;
+(NSDate *)getTimeDate_End:(NSString *)endTime;
+(NSString *)getTimeStampFromDate:(NSDate *)date;
+(NSDate *)getDateFromTimeStamp:(NSString *)timeStamp;
+(NSString *)getTimeFormatStringFromDate:(NSDate *)date;

#pragma mark - get image Encode
+ (NSString *)getEncodeToBase64String:(UIImage *)image;

#pragma mark - check Value
+(CGFloat)getMaxValue:(CGFloat)firstValue withSecondValue:(CGFloat)secondValue;
+(CGFloat)getMinValue:(CGFloat)firstValue withSecondValue:(CGFloat)secondValue;

#pragma mark - NSUserDefaults
+(void)setNSUserDefaultsKey:(NSString *)key andValue:(NSString *)value;
+(NSString *)getStringFromNSUserDefaults:(NSString *)key;
+(NSInteger)getINTFromNSUserDefaults:(NSString *)key;
+(float)getFloatFromNSUserDefaults:(NSString *)key;

#pragma mark - LINE Share
+(void)shareLineWithURL:(NSString *)msgWithURL;
@end
