//
//  MOMOUtils.m
//  ITPUSH
//
//  Created by eyliu on 2014/6/16.
//  Copyright (c) 2014年 eyliu. All rights reserved.
//

#import "MOMOUtils.h"

@implementation MOMOUtils
 NSString *_deviceToken;
NSString *_notificationInfo;
NSString *_notificationName;
NSString *_notificationTime;


+(void)SetDeviceToken:(NSString *)Token{
    _deviceToken = Token;
}


+(NSString *)getDeviceToken{
    return _deviceToken;
}

+(void)SetNotificationInfo:(NSString *)info{
    _notificationInfo = info;
}

+(NSString *)getNotificationInfo{
    return _notificationInfo;
}

+(void)SetNotificationName:(NSString *)name{
    _notificationName = name;
}
+(NSString *)getNotificationName{
    return _notificationName;
}

+(void)SetNotificationTime:(NSString *)time{
    _notificationTime = time;
}
+(NSString *)getNotificationTime{
    return _notificationTime;
}
@end
