//
//  MOMOUtils.h
//  ITPUSH
//
//  Created by eyliu on 2014/6/16.
//  Copyright (c) 2014年 eyliu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MOMOUtils : NSObject
+(void)SetDeviceToken:(NSString *)Token;
+(NSString *)getDeviceToken;

+(void)SetNotificationInfo:(NSString *)info;
+(NSString *)getNotificationInfo;

+(void)SetNotificationName:(NSString *)name;
+(NSString *)getNotificationName;

+(void)SetNotificationTime:(NSString *)time;
+(NSString *)getNotificationTime;

@end
