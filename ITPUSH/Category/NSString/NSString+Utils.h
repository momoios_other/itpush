//
//  NSString+Utils.h
//  Community
//
//  Created by Weiyu Chen on 2015/2/6.
//  Copyright (c) 2015年 Fanzytv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)
- (BOOL) isEmpty;
- (BOOL) isNotEmpty;
@end
