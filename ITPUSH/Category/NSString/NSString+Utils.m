//
//  NSString+Utils.m
//  Community
//
//  Created by Weiyu Chen on 2015/2/6.
//  Copyright (c) 2015年 Fanzytv. All rights reserved.
//

#import "NSString+Utils.h"

#define REGEX_URL @"((http(s)?://[[0-9a-zA-Z]-_]+(\\.[[0-9a-zA-Z]-_]+)+\\.?(:[0-9]+)?|[[0-9a-zA-Z]-_]+(\\.[[0-9a-zA-Z]-_]+)+\\.?(:[0-9]+)?\\/|[\\.[0-9a-zA-Z]-_]+(\\.com|\\.org|\\.edu|\\.gov|\\.tw|fanxin.tv(:[0-9]+)?))([A-Za-z0-9.\\-_~:/?#\\[\\]@!$&'\\(\\)*+,;=]+)?)"
#define REGEX_MAIL @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"

@implementation NSString (Utils)

/**
 *  判斷是否"是空字串"，與 isNotEmpty 成對，視語意使用
 */
- (BOOL) isEmpty{
    return [self.trimmedString isEqualToString:@""];
}

/**
 *  判斷是否"不是空字串"，與 isEmpty 成對，視語意使用
 */
- (BOOL) isNotEmpty{
    return ![self isEmpty];
}

/**
 *  去除文字兩端之空白字元
 */
- (NSString *) trimmedString{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
