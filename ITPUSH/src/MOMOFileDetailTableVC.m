//
//  MOMOPUSHWebVC.m
//  ITPUSH
//
//  Created by eyliu on 2014/6/20.
//  Copyright (c) 2014年 eyliu. All rights reserved.
//

#import "MOMOFileDetailTableVC.h"

#define FONT_SIZE 18.0f
#define CELL_CONTENT_WIDTH 320.0f
#define CELL_CONTENT_MARGIN 10.0f
#define DISPLAY_CELL_NUMBER 30

@interface MOMOFileDetailTableVC (){
    NSMutableArray *contentArray;
    NSMutableArray *timeArray;
    NSMutableArray *isHTMLArray;
    NSMutableArray *HTMLContentArray;
    
    NSArray *arr_reverseallLine;
    NSInteger int_pageNo;
    
    IBOutlet UITableView *detailTable;
}

@end

@implementation MOMOFileDetailTableVC
@synthesize getfileName;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initial];
    [self setGUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activeNotification:) name:@"activeNotification" object:nil];
    
    //Load file
    NSString *filecontent =[self get_fileContent_from_filename:getfileName];
    
    NSArray* arry_allLine = [filecontent componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    arr_reverseallLine = [[arry_allLine reverseObjectEnumerator] allObjects];

    NSUInteger int_MinDisplayNum = (DISPLAY_CELL_NUMBER < [arr_reverseallLine count])?DISPLAY_CELL_NUMBER:[arr_reverseallLine count];
    [self setTableContentBeginIndex:0 andEndIndex:int_MinDisplayNum];
}

-(void)initial{
    contentArray = [[NSMutableArray alloc] init];
    timeArray = [[NSMutableArray alloc] init];
    isHTMLArray = [[NSMutableArray alloc] init];
    HTMLContentArray = [[NSMutableArray alloc] init];
    
    arr_reverseallLine = [[NSArray alloc] init];
    int_pageNo = 1;
    
    self.navigationController.navigationBar.topItem.title = @"返回";
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"5F4B8B"];
}

-(void)setGUI{
    //bar title
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Open Sans" size:21.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithHexString:@"5F4B8B"];
    self.navigationItem.titleView = label;
    label.text = getfileName;
    [label sizeToFit];
    
    detailTable.tableFooterView = [[UIView alloc] init];
}

-(void)setTableContentBeginIndex:(NSInteger)beginCellIndex andEndIndex:(NSInteger)endCellIndex{
    
    for (NSInteger index =beginCellIndex; index<endCellIndex; index++) {
        
        NSString *getCurrentContent  = [arr_reverseallLine objectAtIndex:index];
        NSData *jsonData = [getCurrentContent dataUsingEncoding:NSUTF8StringEncoding];
        NSError *e;
        NSArray *linescontent = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
        
        
        NSLog(@"show linescontent: %@", linescontent);
        
        if (!linescontent) {
            NSLog(@"error parse data");
        }else{
            NSString *getContent = [linescontent objectAtIndex:0];
            
            NSString *getTime;
            if ([[linescontent objectAtIndex:2] length]==14) {
                getTime = [NSString stringWithFormat:@"%@/%@ %@:%@:%@",[[linescontent objectAtIndex:2] substringWithRange:NSMakeRange(4, 2)],[[linescontent objectAtIndex:2] substringWithRange:NSMakeRange(6, 2)],[[linescontent objectAtIndex:2] substringWithRange:NSMakeRange(8, 2)],[[linescontent objectAtIndex:2] substringWithRange:NSMakeRange(10, 2)],[[linescontent objectAtIndex:2] substringWithRange:NSMakeRange(12, 2)]];
            }else{
                //Time format is not correct!
                getTime = @"XX/XX XX:XX:XX";
            }
            
            if ([linescontent count]==5) {
                [isHTMLArray addObject:@"1"];
                [HTMLContentArray addObject:[linescontent objectAtIndex:4]];
            }else{
                [isHTMLArray addObject:@"0"];
                [HTMLContentArray addObject:@""];
            }
            
            [contentArray addObject:getContent];
            [timeArray addObject:getTime];
        }
    }
    
    //set "already read" flag
    [MyUtils setNSUserDefaultsKey:getfileName andValue:@"0"];
    
    [detailTable reloadData];
}

#pragma mark - content
-(NSString *)get_fileContent_from_filename:(NSString *)filename{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentsDirectory = [NSString stringWithFormat:@"%@/%@",[paths objectAtIndex:0],@"msg"];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",filename]];
    
    //check file exist
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]){
        NSLog(@"file:%@ exist", filePath);
        return [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    }else{
        return @"";
    }
}

#pragma mark - Table Setting
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[isHTMLArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
        //[self performSegueWithIdentifier:@"pushHTML" sender:self];
        
        // Get reference to the destination view controller
        MOMOWebVC *vc = [[MOMOWebVC alloc] initWithNibName:@"MOMOWebVC" bundle:nil];

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

        // Pass any objects to the view controller here, like...
        vc.getHTMLParam = [NSString stringWithFormat:@"%@_%d",[userDefaults stringForKey:@"empId"],[[HTMLContentArray objectAtIndex:indexPath.section] intValue]-2013];

        [self.navigationController pushViewController:vc animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [contentArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    // If you're serving data from an array, return the length of the array:
    return 1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"FCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if ([[isHTMLArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    NSLog(@"%@", [contentArray objectAtIndex:indexPath.section]);
    
    // Set the data for this cell:
    cell.textLabel.text = [contentArray objectAtIndex:indexPath.section];
    [cell.textLabel setNumberOfLines:0]; // unlimited number of lines
    [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    
    cell.textLabel.font = [UIFont systemFontOfSize:FONT_SIZE];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *text = [contentArray objectAtIndex:[indexPath section]];
    
    CGFloat width = tableView.frame.size.width - 15 - 30 - 15;  //tableView width - left border width - accessory indicator - right border width
    UIFont *font = [UIFont systemFontOfSize:FONT_SIZE];
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: font}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;
    size.height = ceilf(size.height);
    size.width  = ceilf(size.width);
    return size.height + 15;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 4, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    NSString *string =[timeArray objectAtIndex:section];

    [label setText:string];
    [label setTextColor:[UIColor whiteColor]];
    [view addSubview:label];
    [view setBackgroundColor:[MyUtils SetColorWithHexString:@"5F4B8B" andAlpha:0.9f]];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 24;
}

#pragma mark - notificationsetting
- (void)activeNotification:(NSNotification*)note {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - unlimit scrollview(if necessary)
//當下拉至最底時，增加pageIndex值，重新Query
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if (maximumOffset - currentOffset <= -40) {
        if (DISPLAY_CELL_NUMBER*int_pageNo <= [arr_reverseallLine count]) {
            [self loadmoreData];
        }
    }
}

-(void)loadmoreData{
    int_pageNo += 1;
    
    BOOL isEndContent = DISPLAY_CELL_NUMBER*int_pageNo>[arr_reverseallLine count];
    
    if (isEndContent) {
        [self setTableContentBeginIndex:DISPLAY_CELL_NUMBER*(int_pageNo-1) andEndIndex:[arr_reverseallLine count]];
    }else{
        [self setTableContentBeginIndex:DISPLAY_CELL_NUMBER*(int_pageNo-1) andEndIndex:DISPLAY_CELL_NUMBER*int_pageNo];
    }
    
}

@end
