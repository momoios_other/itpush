//
//  MOMORebootVC.m
//  ITPUSH
//
//  Created by eyliu on 2016/1/15.
//  Copyright © 2016年 momoshop. All rights reserved.
//

#import "MOMORebootVC.h"
#import "MOMOUtils.h"
#import "PureLayout.h"

typedef NS_ENUM(NSInteger, kAlertStreamingMachine) {
    kAlertMachine201 = 201,
    kAlertMachine202 = 202
};

@interface MOMORebootVC (){
    NSString *postString;
    UIButton *btn_RebootStream;
    UIButton *btn_RebootStream2;
}
@end

@implementation MOMORebootVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    btn_RebootStream = [[UIButton alloc] initForAutoLayout];
    [btn_RebootStream.layer setCornerRadius : 4.0f];
    [btn_RebootStream setBackgroundColor:[MyUtils SetColorWithHexString:@"FF7100" andAlpha:1.0f]];
    [btn_RebootStream setTitleColor:[MyUtils SetColorWithHexString:@"FFFFFF" andAlpha:1.0f] forState:UIControlStateNormal];
    [btn_RebootStream setTitle:@"重啟Streaming編碼機201" forState:UIControlStateNormal];
    [btn_RebootStream addTarget:self action:@selector(pressButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_RebootStream];
    [btn_RebootStream autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:130.f];
    [btn_RebootStream autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [btn_RebootStream autoSetDimensionsToSize:CGSizeMake(280.f, 44.f)];
    
    btn_RebootStream2 = [[UIButton alloc] initForAutoLayout];
    [btn_RebootStream2.layer setCornerRadius : 4.0f];
    [btn_RebootStream2 setBackgroundColor:[MyUtils SetColorWithHexString:@"FF7100" andAlpha:1.0f]];
    [btn_RebootStream2 setTitleColor:[MyUtils SetColorWithHexString:@"FFFFFF" andAlpha:1.0f] forState:UIControlStateNormal];
    [btn_RebootStream2 setTitle:@"重啟Streaming編碼機202" forState:UIControlStateNormal];
    [btn_RebootStream2 addTarget:self action:@selector(pressButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_RebootStream2];
    [btn_RebootStream2 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:btn_RebootStream withOffset:50.f];
    [btn_RebootStream2 autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [btn_RebootStream2 autoSetDimensionsToSize:CGSizeMake(280.f, 44.f)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Open Sans" size:21.0];
    //label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
    label.text = @"Streaming編碼機重啟";
    [label sizeToFit];
    
    self.navigationController.navigationBar.topItem.title = @"返回";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)pressButton:(UIButton *)sender{
    if (sender == btn_RebootStream) {
        UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:@"確定要重啟201嗎？" message:@"請輸入密碼，才能執行本功能" delegate:self cancelButtonTitle:@"取消" otherButtonTitles: @"確認", nil];
        alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        alert.tag = kAlertMachine201;
        [alert show];
    }else if(sender == btn_RebootStream2){
        UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:@"確定要重啟202嗎？" message:@"請輸入密碼，才能執行本功能" delegate:self cancelButtonTitle:@"取消" otherButtonTitles: @"確認", nil];
        alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        alert.tag = kAlertMachine202;
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex != [alertView cancelButtonIndex]) {
        UITextField *password = [alertView textFieldAtIndex:0];
        
        if (alertView.tag == kAlertMachine201){
            postString = [NSString stringWithFormat:@"TAG=streamReboot&emp_id=%@&command=reBoot_Streaming&passwd=%@",[MyUtils getStringFromNSUserDefaults:@"empId"], password.text];
            
        }else if (alertView.tag == kAlertMachine202){
            postString = [NSString stringWithFormat:@"TAG=streamReboot&emp_id=%@&command=reBoot_Streaming2&passwd=%@",[MyUtils getStringFromNSUserDefaults:@"empId"], password.text];
        }
    
        [self connect:postString];
    }
}

-(void)connect:(NSString *)postdata{
    NSData *postData = [postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kPHPUrlRebootStreaming]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    //MyLog(@"send Register Data : %@", postString);
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    // Log Response
    NSString *response = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    MyLog(@"get Register response: %@",response);
    
    
    if (response == (id)[NSNull null] || response.length == 0 )  {
        UIAlertView *showResponse = [[UIAlertView alloc] initWithTitle:@"訊息" message:@"回傳訊息有誤" delegate:Nil cancelButtonTitle:NSLocalizedString(@"BUTTON_CONFIRM", @"") otherButtonTitles:Nil];
        showResponse.delegate = self;
        [showResponse show];
    }else if([response rangeOfString:@"OK"].location != NSNotFound){
        UIAlertView *showResponse = [[UIAlertView alloc] initWithTitle:@"訊息" message:@"發送成功" delegate:Nil cancelButtonTitle:NSLocalizedString(@"BUTTON_CONFIRM", @"") otherButtonTitles:Nil];
        showResponse.delegate = self;
        [showResponse show];
        
    }
}

@end
