//
//  MOMOWebVC.h
//  ITPUSH
//
//  Created by eyliu on 2015/2/2.
//  Copyright (c) 2015年 momoshop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOMOWebVC : UIViewController<UIWebViewDelegate>{
    IBOutlet UIWebView *webview;
    IBOutlet UIActivityIndicatorView *m_activity;
}

@property (nonatomic, retain) UIActivityIndicatorView *m_activity;
@property (nonatomic ,strong) NSString *getHTMLParam;

@end
