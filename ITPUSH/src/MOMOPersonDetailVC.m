//
//  MOMOPersonDetailVC.m
//  ITPUSH
//
//  Created by eyliu on 2014/6/26.
//  Copyright (c) 2014年 eyliu. All rights reserved.
//

#import "MOMOPersonDetailVC.h"
#import "PureLayout.h"

#define NUMBERS_ONLY @"1234567890"
#define EMP_LIMIT_MAX_LENGTH 5
#define PHONE_LIMIT_MAX_LENGTH 10
#define PASS_LIMIT_MAX_LENGTH  14

typedef NS_ENUM (NSInteger, TextFieldType) {
    TextFieldTypePhone = 200,
    TextFieldTypeEMPID = 201,
    TextFieldTypeOIS   = 202
};

typedef NS_ENUM (NSInteger, AlertType) {
    AlertTypeReigsterConfirm = 300,
    AlertTypeReigsterDone    = 301
};

@interface MOMOPersonDetailVC ()
@property (nonatomic, strong) UIButton *btn_Register;
@property (nonatomic, strong) UIButton *btn_quit;
@property (nonatomic, strong) NSString *postString;
@property (nonatomic, strong) UITableView *tb_login;
@property (nonatomic, strong) MBProgressHUD *MBPhud;
@end

@implementation MOMOPersonDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initial];
    [self buildNotification];
}

-(void)initial{

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Open Sans" size:21.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithHexString:@"5F4B8B"];
    self.navigationItem.titleView = label;
    label.text = @"帳號資訊";
    [label sizeToFit];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    [self.view setBackgroundColor:[MyUtils SetColorWithHexString:@"EEEEEE" andAlpha:1.0f]];
    
    _tb_login = [[UITableView alloc] init];
    _tb_login.dataSource = self;
    _tb_login.delegate = self;
    _tb_login.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tb_login];
    [_tb_login autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:0.f];
    [_tb_login autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:40.f];
    [_tb_login autoSetDimensionsToSize:CGSizeMake([[UIScreen mainScreen] bounds].size.height, 200)];
    
    _btn_Register = [[UIButton alloc] init];
    [_btn_Register.layer setCornerRadius : 4.0f];
    [_btn_Register setBackgroundColor:[MyUtils SetColorWithHexString:@"FFFFFF" andAlpha:1.0f]];
    [_btn_Register setTitleColor:[MyUtils SetColorWithHexString:@"FF0000" andAlpha:1.0f] forState:UIControlStateNormal];
    _btn_Register.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    [_btn_Register addTarget:self action:@selector(pressButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btn_Register];
    [_btn_Register autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [_btn_Register autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_tb_login withOffset:30.f];
    [_btn_Register autoSetDimensionsToSize:CGSizeMake(200, 40)];
    
    _btn_quit = [[UIButton alloc] init];
    [_btn_quit.layer setCornerRadius : 4.0f];
    [_btn_quit setBackgroundColor:[MyUtils SetColorWithHexString:@"FFFFFF" andAlpha:1.0f]];
    [_btn_quit setTitleColor:[MyUtils SetColorWithHexString:@"000000" andAlpha:1.0f] forState:UIControlStateNormal];
    _btn_quit.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    [_btn_quit addTarget:self action:@selector(pressButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btn_quit];
    [_btn_quit autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [_btn_quit autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_btn_Register withOffset:20.f];
    [_btn_quit autoSetDimensionsToSize:CGSizeMake(200, 40)];
    [_btn_quit setTitle:@"離開" forState:UIControlStateNormal];
    
    if ([MyUtils isEmptyString:[MyUtils getStringFromNSUserDefaults:@"empId"]] && [MyUtils isEmptyString:[MyUtils getStringFromNSUserDefaults:@"personPhone"]]) {
        [_btn_Register setTitle:@"註冊" forState:UIControlStateNormal];
    }else{
        [_btn_Register setTitle:@"重新註冊" forState:UIControlStateNormal];
    }
    
    if ([MyUtils isEmptyString:[MyUtils getStringFromNSUserDefaults:@"empId"]]) {
        self.navigationItem.hidesBackButton = YES;
        _btn_quit.hidden = NO;
    }else{
        _btn_quit.hidden = YES;
    }
    
    self.navigationController.navigationBar.topItem.title = @"返回";
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"5F4B8B"];
    
    _tb_login.tableFooterView = [[UIView alloc] init];
}

#pragma mark - button setting
- (void)pressButton:(UIButton *)sender{
    if (sender == _btn_Register) {
        [self Register];
    }else if(sender == _btn_quit){
        exit(0);
    }
}

#pragma mark - textfield setting
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS_ONLY] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if (textField.tag == TextFieldTypePhone) {
        return (([string isEqualToString:filtered])&&(newLength <= PHONE_LIMIT_MAX_LENGTH));
    }else if (textField.tag == TextFieldTypeEMPID) {
        return (([string isEqualToString:filtered])&&(newLength <= EMP_LIMIT_MAX_LENGTH));
    }else if (textField.tag == TextFieldTypeOIS) {
        return newLength <= PASS_LIMIT_MAX_LENGTH;
    }else{
        return YES;
    }
    
}

#pragma mark - notificationsetting
-(void)buildNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshNotification:) name:@"refreshNotification" object:nil];
}

- (void)refreshNotification:(NSNotification*)note {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table Setting
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if ([indexPath section] == 0) {
            
            UITextField *playerTextField;

            if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
                
                //PLUS 尺寸規格
                if ((int)[[UIScreen mainScreen] nativeBounds].size.height == 1920 || (int)[[UIScreen mainScreen] nativeBounds].size.height == 2208) {
                    playerTextField = [[UITextField alloc] initWithFrame:CGRectMake(160, 8, 245, 30)];
                }else{
                    playerTextField = [[UITextField alloc] initWithFrame:CGRectMake(140, 8, 245, 30)];
                }
            
            }
            
            playerTextField.adjustsFontSizeToFitWidth = YES;
            playerTextField.textColor = [UIColor blackColor];
            playerTextField.delegate = self;
            if ([indexPath row] == 0) {
                playerTextField.placeholder = @"";
                playerTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                playerTextField.tag = TextFieldTypePhone;
                //playerTextField.returnKeyType = UIReturnKeyNext;
                
                if (![MyUtils isEmptyString:[MyUtils getStringFromNSUserDefaults:@"personPhone"]]) {
                    playerTextField.text = [MyUtils getStringFromNSUserDefaults:@"personPhone"];
                }
            }
            else if([indexPath row] == 1){
                playerTextField.placeholder = @"";
                playerTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                playerTextField.tag = TextFieldTypeEMPID;
                
                if (![MyUtils isEmptyString:[MyUtils getStringFromNSUserDefaults:@"empId"]]) {
                    playerTextField.text = [MyUtils getStringFromNSUserDefaults:@"empId"];
                }
                //playerTextField.returnKeyType = UIReturnKeyDone;
            }else if([indexPath row] == 2){
                playerTextField.placeholder = @"";
                playerTextField.keyboardType = UIKeyboardTypeDefault;
                //playerTextField.returnKeyType = UIReturnKeyDone;
                playerTextField.tag = TextFieldTypeOIS;
                playerTextField.secureTextEntry = YES;
            }
            playerTextField.backgroundColor = [UIColor whiteColor];
            playerTextField.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
            playerTextField.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
            playerTextField.textAlignment = NSTextAlignmentLeft;
            //playerTextField.delegate = self;
            
            playerTextField.clearButtonMode = UITextFieldViewModeNever; // no clear 'x' button to the right
            [playerTextField setEnabled: YES];
            
            [cell.contentView addSubview:playerTextField];
        }
    }
    
    [cell setSeparatorInset:UIEdgeInsetsZero];
    if ([indexPath section] == 0) { // Email & Password Section
        if ([indexPath row] == 0) { // Email
            cell.textLabel.font = [UIFont fontWithName:kFontAwesomeFamilyName size:20];
            cell.textLabel.text = [NSString stringWithFormat:@"%@ 手機門號：",[NSString fontAwesomeIconStringForEnum:FAPhone]];
        }
        else if ([indexPath row] == 1){
            cell.textLabel.font = [UIFont fontWithName:kFontAwesomeFamilyName size:20];
            cell.textLabel.text = [NSString stringWithFormat:@"%@ 員工編號：",[NSString fontAwesomeIconStringForEnum:FAUser]];
        }else if ([indexPath row] == 2){
            cell.textLabel.font = [UIFont fontWithName:kFontAwesomeFamilyName size:20];
            cell.textLabel.text = [NSString stringWithFormat:@"%@  OIS 密碼：",[NSString fontAwesomeIconStringForEnum:FALock]];
        }
    }
    return cell;
}

#pragma mark - others
-(void)Register{
    
    UITextField *tfPhone     = (UITextField *)[self.view viewWithTag:TextFieldTypePhone];
    UITextField *tfEMPID     = (UITextField *)[self.view viewWithTag:TextFieldTypeEMPID];
    UITextField *tfOISPASSWD = (UITextField *)[self.view viewWithTag:TextFieldTypeOIS];
    
    if (![tfPhone.text isEqualToString:@""] && ![tfEMPID.text isEqualToString:@""] && ![tfOISPASSWD.text isEqualToString:@""]) {
        _postString = [NSString stringWithFormat:@"regId=%@&mobile=%@&emp_id=%@&passwd=%@",[MOMOUtils getDeviceToken],tfPhone.text,tfEMPID.text,tfOISPASSWD.text];
        
        MyLog(@"token id: %@:", [MOMOUtils getDeviceToken]);
        //將使用\n手機號碼：%@\n員工編號：%@\n進行註冊，是否確定？
        NSString *thisdialog = [NSString stringWithFormat:@"%@%@%@%@%@",NSLocalizedString(@"DIALOG_CONFIRM_FILLDATA_1", @""),tfPhone.text,NSLocalizedString(@"DIALOG_CONFIRM_FILLDATA_2", @""),tfEMPID.text,NSLocalizedString(@"DIALOG_CONFIRM_FILLDATA_3", @"")];
        UIAlertView *showURL = [[UIAlertView alloc] initWithTitle:@"訊息" message:thisdialog delegate:Nil cancelButtonTitle:NSLocalizedString(@"BUTTON_CANCEL", @"") otherButtonTitles:NSLocalizedString(@"BUTTON_CONFIRM", @""),Nil];
        showURL.tag = AlertTypeReigsterConfirm;
        showURL.delegate = self;
        [showURL show];
    }else{
        //請填入員工編號及手機號碼
        UIAlertView *showURL = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DIALOG_LACK_EMPIDPHONENUMBEr", @"") message:@"" delegate:Nil cancelButtonTitle:NSLocalizedString(@"BUTTON_CONFIRM", @"") otherButtonTitles:Nil];
        showURL.tag = AlertTypeReigsterConfirm;
        showURL.delegate = self;
        [showURL show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag ==AlertTypeReigsterConfirm && buttonIndex != [alertView cancelButtonIndex]) {
        
        NSData *postData = [_postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[_postString length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kPHPUrlRegister]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
        
        //MyLog(@"send Register Data : %@", _postString);
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        // Log Response
        NSString *response = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
        MyLog(@"get Register response: %@",response);
        
        
        if (response == (id)[NSNull null] || response.length == 0 )  {
            //回傳訊息有誤，請檢查網路環境！
            UIAlertView *showResponse = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WORD_ERROR", @"") message:NSLocalizedString(@"DIALOG_NETWORK_ENVERROR", @"") delegate:Nil cancelButtonTitle:NSLocalizedString(@"BUTTON_CONFIRM", @"") otherButtonTitles:Nil];
            showResponse.delegate = self;
            [showResponse show];
        }else if([response rangeOfString:@"ERR"].location != NSNotFound){
            //註冊訊息有誤，聯絡管理者！
            UIAlertView *showResponse = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WORD_ERROR", @"") message:NSLocalizedString(@"DIALOG_NETWORK_INFOERROR", @"") delegate:Nil cancelButtonTitle:NSLocalizedString(@"BUTTON_CONFIRM", @"") otherButtonTitles:Nil];
            showResponse.delegate = self;
            [showResponse show];
        }else if([response rangeOfString:@"OK"].location != NSNotFound){
            
            UITextField *tfPhone = (UITextField *)[self.view viewWithTag:TextFieldTypePhone];
            UITextField *tfEMPID = (UITextField *)[self.view viewWithTag:TextFieldTypeEMPID];
            
            [MyUtils setNSUserDefaultsKey:@"personPhone" andValue:tfPhone.text];
            [MyUtils setNSUserDefaultsKey:@"empId" andValue:tfEMPID.text];
            
            //註冊完成！
            UIAlertView *showResponse = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WORD_SUCCESS", @"") message:NSLocalizedString(@"DIALOG_REGISTER_SUCCESS", @"") delegate:Nil cancelButtonTitle:NSLocalizedString(@"BUTTON_CONFIRM", @"") otherButtonTitles:Nil];
            showResponse.tag = AlertTypeReigsterDone;
            showResponse.delegate = self;
            [showResponse show];
        }
        
    }else if (alertView.tag == AlertTypeReigsterDone){
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) showHUDAddedTo:(UIView *)view
               animated:(BOOL)animated
                   text:(NSString *)text
            delayToHide:(CGFloat)delayToHide
{
    _MBPhud = [MBProgressHUD showHUDAddedTo:view animated:animated];
    _MBPhud.labelText = text;
    [_MBPhud show:YES];
    if (delayToHide > 0) {
        [_MBPhud hide:YES afterDelay:delayToHide];
    }
}

@end
