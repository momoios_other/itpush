//
//  MOMOWebVC.m
//  ITPUSH
//
//  Created by eyliu on 2015/2/2.
//  Copyright (c) 2015年 momoshop. All rights reserved.
//

#import "MOMOWebVC.h"

@interface MOMOWebVC ()

@end

@implementation MOMOWebVC
@synthesize getHTMLParam;
@synthesize m_activity;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    webview.delegate = self;
    //[webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",getWebURL]]]];
    MyLog(@"Fetch file : %@", getHTMLParam);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",getHTMLParam]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:filePath]){
        [self getiGetHTMLInfo];
    }else{
        [self showHTML:getHTMLParam];
    }
    
    self.navigationController.navigationBar.topItem.title = @"返回";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    m_activity.hidden= TRUE;
    [m_activity stopAnimating];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    m_activity.hidden= FALSE;
    [m_activity startAnimating];
}


-(void)getiGetHTMLInfo{
    
    NSString *postString = [NSString stringWithFormat:@"FILEID=%@",getHTMLParam];
    
    NSData *postData = [postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kPHPUrlQueryMessageHTMLFile]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    // Log Response
    NSString *response = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    MyLog(@"response: %@",response);
    
    [self saveHTML:response];
}

-(void)saveHTML:(NSString *)saveHTML{
    // save message to personal file
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [NSString stringWithFormat:@"%@/%@",[paths objectAtIndex:0],@"html"];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",getHTMLParam]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:filePath]){
        [saveHTML writeToFile:filePath atomically:TRUE encoding:NSUTF8StringEncoding error:NULL];
    }
    
    [self showHTML:getHTMLParam];
}


-(void)showHTML:(NSString *)getHTML{
    MyLog(@"get HTML content: %@",[self get_fileContent_from_filename:getHTMLParam]);
    [webview loadHTMLString:[self get_fileContent_from_filename:getHTMLParam] baseURL:nil];
    
    [webview setScalesPageToFit:YES];
}

-(NSString *)get_fileContent_from_filename:(NSString *)filename{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [NSString stringWithFormat:@"%@/%@",[paths objectAtIndex:0],@"html"];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",filename]];
    
    return [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
}

@end
