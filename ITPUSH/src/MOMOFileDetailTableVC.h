//
//  MOMOPUSHWebVC.h
//  ITPUSH
//
//  Created by eyliu on 2014/6/20.
//  Copyright (c) 2014年 eyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOMOWebVC.h"

@interface MOMOFileDetailTableVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) NSString *getfileName;

@end
