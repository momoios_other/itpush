//
//  MOMOConfig.h
//  ITPUSH
//
//  Created by Macel on 2014/9/27.
//  Copyright (c) 2014年 eyliu. All rights reserved.
//

#ifndef ITPUSH_MOMOConfig_h
#define ITPUSH_MOMOConfig_h

static NSString* const kPHPUrlRegister              = @"http://ec-monitor.fmt.com.tw:1688/gcm/service.php?ID=iosTest&TAG=iReg";
static NSString* const kPHPUrlQueryMessageFile      = @"http://ec-monitor.fmt.com.tw:1688/gcm/service.php?ID=iosTest&TAG=iGet";
static NSString* const kPHPUrlQueryMessageHTMLFile  = @"http://ec-monitor.fmt.com.tw:1688/gcm/service.php?ID=iosTest&TAG=iGetHTML";

static NSString* const kPHPUrlRebootStreaming       = @"http://ec-monitor.fmt.com.tw:1688/gcm/service.php?ID=momoIT";

#endif
