//
//  MOMOPersonDetailVC.h
//  ITPUSH
//
//  Created by eyliu on 2014/6/26.
//  Copyright (c) 2014年 eyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOMOUtils.h"
#import "NSString+FontAwesome.h"

@interface MOMOPersonDetailVC : UIViewController<UITextFieldDelegate,UIAlertViewDelegate,UITableViewDataSource, UITableViewDelegate>

@end
