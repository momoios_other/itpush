//
//  MOMOMainTableVC.m
//  ITPUSH
//
//  Created by eyliu on 2015/1/6.
//  Copyright (c) 2015年 momoshop. All rights reserved.
//

#import "MOMOMainTableVC.h"

typedef NS_ENUM (NSInteger, AlertType) {
    AlertTypeRegister = 0
};

@interface MOMOMainTableVC (){
    IBOutlet UITableView *mainTable;
    IBOutlet UINavigationItem *MainTitle;
    NSMutableArray *nameArray;
    NSMutableArray *latestContentArray;
    NSMutableArray *timeArray;
    NSMutableArray *time2Array;
    NSMutableArray *resultArray;
    
    UIRefreshControl *refreshControl;
}
@end

@implementation MOMOMainTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initNavBar];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [mainTable addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable:) forControlEvents:UIControlEventValueChanged];
    
    if ([MyUtils isEmptyString:[MyUtils getStringFromNSUserDefaults:@"empId"]]) {
        MyLog(@"not Register");
        [self showRegDialog];
    }else{
        MyLog(@"already Reigster....");
    }

    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    NSLog(@"get size : %f %f", width, height);
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification:) name:@"arriveNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification:) name:@"activeNotification" object:nil];
    [self getMessageFileInfo];
    mainTable.tableFooterView = [[UIView alloc] init];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"arriveNotification" object:Nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"activeNotification" object:Nil];
}

- (void)initNavBar{
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"5F4B8B"];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Open Sans" size:21.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithHexString:@"5F4B8B"];
    self.navigationItem.titleView = label;
    label.text = @"富邦媒體GCM";
    [label sizeToFit];

    UIBarButtonItem *btnInfo = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(showActionSheet:)];
    self.navigationController.topViewController.navigationItem.rightBarButtonItem = btnInfo;
    btnInfo.enabled=TRUE;
    
}


#pragma mark - content setting
-(NSString *)get_LastLine_From_content:(NSString *)content{
    NSArray* allLine = [content componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    return [allLine objectAtIndex:[allLine count]-1];
}

-(NSString *)get_fileContent_from_filename:(NSString *)filename{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [NSString stringWithFormat:@"%@/%@",[paths objectAtIndex:0],@"msg"];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",filename]];
    
    return [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
}

-(NSMutableArray *)get_ArrayOfIndex_with_TimeLine_Sorting:(NSArray *)filelist{
    NSMutableArray *sortArray = [[NSMutableArray alloc] init];
    NSMutableArray *originalArray = [[NSMutableArray alloc] init];
    
    // get time sorting line
    for (int i = 0; i < [filelist count]; i++){
        //get file data
        NSString *filecontent = [self get_fileContent_from_filename:[filelist objectAtIndex: i]];
        NSData *LastLine_jsonData = [[self get_LastLine_From_content:filecontent] dataUsingEncoding:NSUTF8StringEncoding];
        NSError *e;
        NSArray *LastLine_Message_Array = [NSJSONSerialization JSONObjectWithData:LastLine_jsonData options:kNilOptions error:&e];
        
        [sortArray addObject:[LastLine_Message_Array objectAtIndex:2]];
        
    }
    originalArray = sortArray;
    
    //get sorted array
    NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:Nil ascending:NO];
    sortArray = [sortArray sortedArrayUsingDescriptors:@[sd]];
    
    //compare sorted array and original array , get increasing index
    for (int i=0; i< sortArray.count; i++) {
        for (int j=0; j< originalArray.count; j++) {
            if ([sortArray[i] isEqualToString: originalArray[j]]) {
                [resultArray addObject:[NSString stringWithFormat:@"%d",j]];
            }
        }
        
    }
    
    return resultArray;
}

-(void)getMessageFileInfo{
    
    nameArray = [[NSMutableArray alloc] init];
    latestContentArray = [[NSMutableArray alloc] init];
    timeArray = [[NSMutableArray alloc] init];
    time2Array = [[NSMutableArray alloc] init];
    resultArray = [[NSMutableArray alloc] init];
    
    //get list from directory
    NSString *documentdir = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],@"msg"];
    
    NSString *tileDirectory = [documentdir stringByAppendingPathComponent:@"/"];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *filelist = [filemgr contentsOfDirectoryAtPath: tileDirectory error: nil];
    
    resultArray = [self get_ArrayOfIndex_with_TimeLine_Sorting:filelist];
    
    for (int k=0; k< resultArray.count; k++) {
        
        //MyLog(@"%@", [filelist objectAtIndex: [resultArray[k] intValue]]);
        
        //get file data
        NSString *filecontent = [self get_fileContent_from_filename:[filelist objectAtIndex: [resultArray[k] intValue]]];
        
        NSData *data = [filecontent dataUsingEncoding:NSUTF8StringEncoding];
        NSString *decodevalue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        
        //MyLog(@"filecontent: %@",decodevalue);
        
        NSData *LastLine_jsonData = [[self get_LastLine_From_content:filecontent] dataUsingEncoding:NSUTF8StringEncoding];
        NSError *e;
        NSArray *LastLine_Message_Array = [NSJSONSerialization JSONObjectWithData:LastLine_jsonData options:kNilOptions error:&e];
        
        NSString *getContent = [LastLine_Message_Array objectAtIndex:0];
        NSString *getTime = [NSString stringWithFormat:@"%@:%@:%@",[[LastLine_Message_Array objectAtIndex:2] substringWithRange:NSMakeRange(8, 2)],[[LastLine_Message_Array objectAtIndex:2] substringWithRange:NSMakeRange(10, 2)],[[LastLine_Message_Array objectAtIndex:2] substringWithRange:NSMakeRange(12, 2)]];
        
        [nameArray addObject:[filelist objectAtIndex:[resultArray[k] intValue]]];
        [latestContentArray addObject:getContent];
        
        [timeArray addObject:getTime];
        [time2Array addObject:[[LastLine_Message_Array objectAtIndex:2] substringWithRange:NSMakeRange(0, 8)]];
    }
    
    [mainTable reloadData];
}

#pragma mark - Table Setting
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSIndexPath *selectedIndexPath = indexPath;
    NSInteger rowNumber = selectedIndexPath.row;
    
    MOMOFileDetailTableVC *DetailVC = [[MOMOFileDetailTableVC alloc] initWithNibName:@"MOMOFileDetailTableVC" bundle:nil];
    DetailVC.getfileName = [nameArray objectAtIndex:rowNumber];
    [self.navigationController pushViewController:DetailVC animated:YES];

    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    // If you're serving data from an array, return the length of the array:
    return [nameArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"CustomCell" bundle:Nil] forCellReuseIdentifier:@"Cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    }
    
    // Set the data for this cell:
    if ([[nameArray objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"WORD_JUDGE_SYSTEM_NAME_1", @"")] ||[[nameArray objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"WORD_JUDGE_SYSTEM_NAME_2", @"")]) {
        cell.userImage.image = [UIImage imageNamed:@"Apple-icon.png"];
    }else{
        cell.userImage.image = [UIImage imageNamed:@"head2.jpeg"];
    }
    cell.userName.text = [nameArray objectAtIndex:indexPath.row];
    
    if([NSNull null] != [latestContentArray objectAtIndex:indexPath.row]) {
        cell.contents.text = [latestContentArray objectAtIndex:indexPath.row];
    }
    
    //[cell.contents sizeToFit];
    
    //original
    //cell.sendTime.text = [timeArray objectAtIndex:indexPath.row];
    
    //new
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyyMMdd"];
    NSDate *startDate = [f dateFromString:[time2Array objectAtIndex:indexPath.row]];
    NSDate *nowdate = [NSDate date];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:nowdate
                                                         options:0];
    NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
    [weekday setDateFormat: @"EEEE"];
    
    if ([components day] <1) {
        cell.sendTime.text = [[timeArray objectAtIndex:indexPath.row] substringToIndex:5];
    }else if([components day] <7 && [components day] >= 1){
        cell.sendTime.text = [NSString stringWithFormat:@"%@", [weekday stringFromDate:startDate]];
    }else{
        cell.sendTime.text = [NSString stringWithFormat:@"%1d%@", [components day],NSLocalizedString(@"WORD_MESSAGE_TIME", @"")];
    }
    
    cell.userImage.layer.cornerRadius = cell.userImage.frame.size.width /2;
    cell.userImage.clipsToBounds = YES;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([[userDefaults stringForKey:[nameArray objectAtIndex:indexPath.row]] isEqualToString:@"1"]) {
        cell.MessageImageView.hidden = NO;
    }else{
        cell.MessageImageView.hidden = YES;
    }

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *filepath = [NSString stringWithFormat:@"%@/%@/%@",[paths objectAtIndex:0],@"msg",[nameArray objectAtIndex:indexPath.row]];
        [fileMgr removeItemAtPath:filepath error:NULL];
        
        [nameArray removeObjectAtIndex:indexPath.row];
        [latestContentArray removeObjectAtIndex:indexPath.row];
        [timeArray removeObjectAtIndex:indexPath.row];
        [resultArray removeObjectAtIndex:indexPath.row];
        
        
        [tableView reloadData]; // tell table to refresh now
    }
}

- (void)refreshTable:(UIRefreshControl *)refreshCtl {
    [refreshCtl endRefreshing];
    [self getMessageFileInfo];
    [mainTable reloadData];
}


#pragma mark - notificationsetting
- (void)handleNotification:(NSNotification*)note {
    [self getMessageFileInfo];
    [mainTable reloadData];
}

#pragma mark - others
-(void) showActionSheet:(id)sender{
    if (_menu.isOpen)
        return [_menu close];
    
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
    
    REMenuItem *homeItem = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"BUTTON_BASIC_SETTING_REIGSTER", @"")
                                                    subtitle:Nil
                                                       image:nil
                                            highlightedImage:nil
                                                      action:^(REMenuItem *item) {
                                                          MOMOPersonDetailVC *DetailVC = [[MOMOPersonDetailVC alloc] initWithNibName:@"MOMOPersonDetailVC" bundle:nil];
                                                          [self.navigationController pushViewController:DetailVC animated:YES];
                                                      }];
    
    REMenuItem *exploreItem = [[REMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"BUTTON_VERSION_INFO", @""),version]
                                                       subtitle:Nil
                                                          image:Nil
                                               highlightedImage:nil
                                                         action:^(REMenuItem *item) {
                                                             
                                                         }];
//    REMenuItem *rebootItem = [[REMenuItem alloc] initWithTitle:@"Streaming編碼機重啟"
//                                                       subtitle:Nil
//                                                          image:Nil
//                                               highlightedImage:nil
//                                                         action:^(REMenuItem *item) {
//                                                             MOMORebootVC *rebootvc = [[MOMORebootVC alloc] initWithNibName:@"MOMORebootVC" bundle:nil];
//                                                             [self.navigationController pushViewController:rebootvc animated:YES];
//                                                         }];
    
    _menu = [[REMenu alloc] initWithItems:@[homeItem, exploreItem]];
    _menu.imageOffset = CGSizeMake(5, -1);
    
    [_menu showFromNavigationController:self.navigationController];


}

-(void)showRegDialog{
    //要先註冊哦！
    UIAlertView *RegView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"BUTTON_SETTING", @"") message:NSLocalizedString(@"DIALOG_NOTI_REGISTER", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"BUTTON_QUIT", @"") otherButtonTitles:NSLocalizedString(@"BUTTON_REGISTER", @""),Nil];
    RegView.tag = AlertTypeRegister;
    [RegView show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag ==AlertTypeRegister) {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            exit(0);
        }else{
            MOMOPersonDetailVC *DetailVC = [[MOMOPersonDetailVC alloc] initWithNibName:@"MOMOPersonDetailVC" bundle:nil];
            [self.navigationController pushViewController:DetailVC animated:YES];
        }
    }
    
}

@end
