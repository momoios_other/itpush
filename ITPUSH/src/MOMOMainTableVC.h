//
//  MOMOMainTableVC.h
//  ITPUSH
//
//  Created by eyliu on 2015/1/6.
//  Copyright (c) 2015年 momoshop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOMOUtils.h"
#import "CustomCell.h"
#import "MOMOFileDetailTableVC.h"
#import "MOMOPersonDetailVC.h"
#import "REMenu.h"
#import "MOMORebootVC.h"

@interface MOMOMainTableVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate, UIActionSheetDelegate>
@property (strong, nonatomic) REMenu *menu;

@end
